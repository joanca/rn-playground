import React from 'react';
import { render } from '@testing-library/react-native';

import { App } from 'src/App';

it('renders correctly', async () => {
  const { getByText, debug } = render(<App />)

  debug()

  expect(getByText(/app/i)).toBeDefined()
});
