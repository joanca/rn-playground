import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
  verbose: true,
  preset: 'react-native',
  testEnvironment: 'node',
  setupFilesAfterEnv: ["@testing-library/jest-native/extend-expect"],
  moduleDirectories: ['node_modules', '.'],
  moduleFileExtensions: [
    "ts",
    "tsx",
    "js",
    "jsx",
    "json",
    "node"
  ],
  globals: {
    "ts-jest": {
      tsconfig: "tsconfig.test.json",
    },
  },
  transform: {
    '^.+\\.(ts|tsx)?$': 'ts-jest',
  }
};

export default config;
